$(document).on("click","#start",function(){

    if($("#running").val() == "false"){
        $("#start").prop("disabled",true);
        $("#currentButton").val("0");
        addtoSerie();
    }
});


$(document).on('click','.box', function(){

    var current = $("#currentButton").val();
    var colores = ["green","red","blue","yellow"];

    if ($("#serie").val()!="" && colores.indexOf($(this).attr('id'))==$("#serie").val().charAt(current)){

        if(current < $("#serie").val().length-1){
            $("#currentButton").val(Number($("#currentButton").val())+1);
        }
        else{
            $(".game-points").text($("#serie").val().length);
            $("#currentButton").val("0");
            addtoSerie();
        }
    }
    else if ($("#running").val() == "true"){
        gameOver();
    }
});


function addtoSerie(){
    var randomNumber = Math.floor(Math.random() * (4 - 0));
    $("#serie").attr("value",$("#serie").val()+randomNumber);
    $("#running").attr("value","false");
    showSerie();
}


function showSerie(){

    var breakpoint = $("#serie").val().length+1;
    var colores = ["#green","#red","#blue","#yellow"];
    var timer;

    for(var i=1; i<breakpoint; i++){
        var color = colores[Number($("#serie").val().charAt(i-1))];
        timer=500*i;

        blink(color,timer);
        
        if (i+1 == breakpoint){
            setTimeout(function() {
                $("#running").attr("value","true");
            }, timer+300);
        }
    }
}

//SACANDO EL SETTIMEOUT DEL BUCLE, CUANDO LE PASAMOS EL VALOR COLOR, AUNQUE SE SOBREESCRIBE, SE QUEDA GUARDADO PARA CADA UNA DE LAS ITERACIONES
function blink(color, timer){
    setTimeout(function() {
        $(color).addClass("active");

        setTimeout(function() {
            $(color).removeClass("active");
        }, 200);
    }, timer);
}

function gameOver(){
    $("#running").val("false");
    $("#currentButton").val("");
    $("#serie").val("");
    $(".game-points").text("0");
    $("#start").prop("disabled",false);
}


/* Hard mode
setTimeout(function() {
    color.addClass("active");

    setTimeout(function() {
        color.removeClass("active");
    }, 200);
}, timer);
*/